var AWS = require('aws-sdk');
AWS.config.region = 'us-west-2';

var fs = require('fs');
var filename = "test.jpg";
var bucket = 'u-tad-alvaro-arranz-project';

var s3obj = new AWS.S3({params: {ACL:'public-read', Bucket:bucket, Key:filename}});
var body = fs.createReadStream(filename);

s3obj.upload({Body: body}).on('httpUploadProgress', function(evt) {

  console.log(evt);
}).send(function(err, data) {

  console.log(err, data)}
);

