
fs = require('fs');
var xml2js = require('xml2js');

var xmlFile = process.argv[2];
var jsonFile = process.argv[3];

var buffer = fs.readFileSync(xmlFile);
var xmlString = buffer.toString();

var parser = new xml2js.Parser();

var parseChannel = function(channelObj) {
	
	var channel = new Object();

	channel.id = channelObj.$.id;
	
	 if (channelObj.hasOwnProperty("display-name")) {
		if (channelObj["display-name"].length > 0) {
			channel.displayName = channelObj["display-name"][0]._;
		}
	}

	if (channelObj.hasOwnProperty("icon")) {
		channel.image = channelObj.icon[0].$.src;
	}
	
	return channel;
}

var parseProgramme = function(programmeObj) {
	
	var programme = new Object();

	programme.start = Number(programmeObj.$.start.split(" ")[0]);
	programme.stop = Number(programmeObj.$.stop.split(" ")[0]);
	programme.channel = programmeObj.$.channel;
	programme.title = programmeObj.title[0]._;
	
	if (programmeObj.hasOwnProperty("sub-title")) {
		programme.subTitle = programmeObj["sub-title"][0]._;
	}
	programme.desc = programmeObj.desc[0]._;

	if (programmeObj.hasOwnProperty("credits")) {
		programme.credits = programmeObj.credits[0];
	}

	programme.category = [];
	
	if (programmeObj.hasOwnProperty("category")) {
		for (var i = 0; i < programmeObj.category.length; i++) {
			programme.category.push(programmeObj.category[i]._);
		}
	}

	if (programmeObj.hasOwnProperty("rating")) {
		programme.rating = programmeObj.rating[0].value[0];
	}

	if (programmeObj.hasOwnProperty("icon")) {
		programme.image = programmeObj.icon[0].$.src;
	}
	
	return programme;
}


parser.parseString(xmlString, function(err, result) {
  if (err) {
    console.log(err);
  } else {
		
		var tvapp = new Object();
		tvapp.channels = [];
		tvapp.programmes = [];

		//console.log(JSON.stringify(result, undefined, 2));

		if (result.tv.hasOwnProperty("channel")) {
			for (var j = 0; j < result.tv.channel.length; j++) {
				var channelObj = result.tv.channel[j];
				var channel = parseChannel(channelObj);
				tvapp.channels.push(channel);
			}
		}
	
		if (result.tv.hasOwnProperty("programme")) {
			for (var j = 0; j < result.tv.programme.length; j++) {
				var programmeObj = result.tv.programme[j];
				var programme = parseProgramme(programmeObj);
				tvapp.programmes.push(programme);
			}
		}

		fs.writeFileSync(jsonFile, JSON.stringify(tvapp, undefined, 2));
		console.log("Done");

    //fs.writeFileSync(jsonFile, JSON.stringify(result, undefined, 2));
    //console.log('Done');
 }
});


