var fs = require('fs');
var AWS = require('aws-sdk');
AWS.config.loadFromPath('awsConfig.json');
var bucket = 'tvapp-bucket';
var imagesFolder = "./images/";
var path = require('path');
var request = require('request');
var gm = require('gm').subClass({ imageMagick: true });

var jpegQuality = 10;
var jpegResizedWidth = 200;

var jsonFile = process.argv[2];
var jsonFileLinks = process.argv[3];

// Read the JSON documment
var buffer = fs.readFileSync(jsonFile);
var jsonString = buffer.toString();
var json = JSON.parse(jsonString);

var pulverizeImage = function(filename, callback) {

  var originalImage = imagesFolder + filename;
  var pulverizedImage = originalImage;

  gm(originalImage).quality(jpegQuality).write(pulverizedImage, function(error) {
    callback(error, pulverizedImage);
  });
}

var resizeImage = function(filename, callback) {

  var originalImage = imagesFolder + filename;

  var splitted = filename.split('.');
  var file = splitted[0];
  var ext = splitted[1];

  var resizedImage = imagesFolder + file + "_tiny." + ext;

  gm(originalImage).resize(jpegResizedWidth).write(resizedImage, function(error) {
    callback(error, file + "_tiny." + ext);
  });
}

var uploadToKuasars = function(filename, callback) {
  
  var s3obj = new AWS.S3({params: {ACL:'public-read', Bucket:bucket, Key:filename}});
  var body = fs.createReadStream(imagesFolder + filename);

  console.log("Uploading to S3... " + imagesFolder + filename);

  s3obj.upload({Body: body}).on('httpUploadProgress', function(evt) {
    console.log(evt);
  }).send(function(err, data) {
    callback(err, data);
  });
}

var download = function(uri, filename, callback) {

  request.head(uri, function(err, res, body) {
    if (err) {
      console.log(err);
      downloadCounter--;
    }
    console.log('content-type:', res.headers['content-type']);
    console.log("file: " + filename);

    request(uri).pipe(fs.createWriteStream(imagesFolder + filename)).on('close', function(err, data) {
      callback(err, data, filename, uri);
    });
  });

};

var downloadCounter = 0;

// add tiny images
for (var i = 0; i < json.programmes.length; i++) {
  var prog = json.programmes[i];
  var filename = path.basename(prog.image);

  if(typeof prog.image === 'undefined'){
    continue;
  };

  splitted = prog.image.split('.');  
  extension = splitted[splitted.length - 1];

  prog.image_tiny = prog.image.replace(extension, '') + "tiny";
}

jsonString = JSON.stringify(json);

for (var i = 0; i < json.programmes.length; i++) {
  var prog = json.programmes[i];
  var filename = path.basename(prog.image);

  if(typeof prog.image === 'undefined'){
    continue;
  };

  downloadCounter++;

  console.log("downloading url: " + prog.image); 
  download(prog.image, filename, function(err, data, file, originalUri) {
    downloadCounter--;
    if (err) {
      console.log(err);
    }
    else {
      pulverizeImage(file, function(error, pulverized) {
        if (error) {
          console.error(error);
        } else {
          
          resizeImage(file, function(err, filename_tiny) {
            if (err) {
              console.log(err);
            } else {

              uploadToKuasars(filename_tiny, function(err, data) {
                if (err)
  	          console.log(err);
	        if (data) {
	          console.log(data);  

                  splitted = originalUri.split('.');  
                  extension = splitted[splitted.length - 1];
                  replaceString = originalUri.replace(extension, '') + "tiny";

                  var re = new RegExp(replaceString, 'g');
	          jsonString = jsonString.replace(re, data.Location);

	          if (downloadCounter <= 0) {
	            fs.writeFileSync(jsonFileLinks, jsonString);
	          }
	        }
              });
            }
          });
          
          uploadToKuasars(file, function(err, data) {
            if (err)
  	      console.log(err);
	    if (data) {
	      console.log(data);
              var re = new RegExp(originalUri, 'g');
	      jsonString = jsonString.replace(re, data.Location);

	      if (downloadCounter <= 0) {
	        fs.writeFileSync(jsonFileLinks, jsonString);
	      }
	    }
          });
        }
      });
    }
  });
}

