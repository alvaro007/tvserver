
var MongoClient = require('mongodb').MongoClient;
var fs = require('fs');

var jsonFile = process.argv[2];
var url = 'mongodb://localhost:27017/tvapp';

// Read the JSON documment
var buffer = fs.readFileSync(jsonFile);
var jsonString = buffer.toString();
jsonString = jsonString.replace(/\$/g, 't');
var json = JSON.parse(jsonString);

MongoClient.connect(url, function(err, db) {
  if (err) {
    console.log(err);
  } else {
    console.log("Connected to mongo correctly");
    
    var collectionChannels = db.collection('channels');

    // save the channels
    for (var i = 0; i < json.channels.length; i++) {
      collectionChannels.update({id:json.channels[i].id}, json.channels[i], {upsert:true}, function(err, result) {
        if (err) {
          console.log(err);
        }
      });  
    }    

    var collectionProgrammesScheduled = db.collection('programmesScheduled');
    // save the programmes
    for (var i = 0; i < json.programmes.length; i++) {
      var prog = json.programmes[i];
      collectionProgrammesScheduled.update({title:prog.title, start:prog.start, stop:prog.stop, channel:prog.channel}, 
                                   prog, {upsert:true}, function (err, result) {
        if (err) {
          console.log(err);
        }
      });
    } 

    var collectionProgrammes = db.collection("programmes");
    for (var i = 0; i < json.programmes.length; i++) {
      var prog = json.programmes[i];
      delete prog.start;
      delete prog.stop;
      delete prog.channel;

      collectionProgrammes.update({title:prog.title}, prog, {upsert:true}, function(err, result) {
        if (err) {
          console.log(err);
        }
      });
    }
    
    // save categories
    var collectionCategories = db.collection("categories");
    for (var i = 0; i < json.programmes.length; i++) {
        var prog = json.programmes[i];
        var categories = prog.category;
        
        if (!categories.length)
            continue;

        for (var j = 0; j < categories.length; j++) {
            var category = categories[j];
            
            var categoryObject = {
                name: category
            }

            collectionCategories.update({name:category}, categoryObject, {upsert:true}, function (err, result) {
                if (err) {
                    console.log(err);
                }
            });
        }
    }
    
    setTimeout(function() {
      db.close();
    }, 10000);
  }
});
