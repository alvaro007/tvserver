
var fs = require('fs');
var http = require('http');
var md5 = require('MD5');

var pioAppKey = "gXsM61kBOFQvxD2qxVfxdFCtZJF7Gueyyzlk8T8YKrLRlstfhyqVhQCmlxKWuwgU";

var post_options = {
    host: "104.155.52.39",
    port: "7070",
    path: "/events.json?accessKey=" + pioAppKey,
    method: "POST",
    headers: {
        'Content-Type': 'application/json'
    }
}   

var jsonFile = process.argv[2];
var buffer = fs.readFileSync(jsonFile);
var jsonString = buffer.toString();
jsonString = jsonString.replace(/\$/g, 't');
var json = JSON.parse(jsonString);

for (var i = 0; i < json.programmes.length; i++) {   

    var programme = json.programmes[i];    
    var now = new Date();
    
    // Sending event related to the Programme
    var pioEventData = {
        event: "$set",
        entityType: "item",
        entityId: md5(programme.title),
        eventTime: now,
        properties: {
            type: "programme",
            name: programme.title
        }
    };

    console.log(JSON.stringify(pioEventData));
    
    var responseCallback = function(res) {
        
        if (!res) {
            console.log("Http request FAILED");
        } else {
        
            res.on('data', function (chunk) {
                console.log('Response: ' + chunk);
            });
        }
    };

    var post_req = http.request(post_options, responseCallback);

    post_req.write(JSON.stringify(pioEventData));
    post_req.end();

    // Sending events related to the Categories
    var categories = programme.category;
    for (var j = 0; j < categories.length; j++) {
        var category = categories[j];

        var pioCategoryEventData = {
            event: "$set",
            entityType: "user",
            entityId: md5(category),
            eventTime: now,
            properties: {
                type: "category",
                name: category
            }
        }
        
        var post_req_category = http.request(post_options, responseCallback);

        post_req_category.write(JSON.stringify(pioCategoryEventData));
        post_req_category.end();

        
        // Sending the view event (for relating the programme with the category)
        var pioViewEventData = {
            event: "view",
            entityType: "user",
            entityId: md5(category),
            targetEntityType: "item",
            targetEntityId: md5(programme.title),
            eventTime: now
        }

        var post_req_view = http.request(post_options, responseCallback);
        
        post_req_view.write(JSON.stringify(pioViewEventData));
        post_req_view.end();
    }

}



