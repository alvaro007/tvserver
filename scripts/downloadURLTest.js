
var fs = require('fs');
var request = require('request');
var gm = require('gm');

var uri = 'http://upload.wikimedia.org/wikipedia/commons/6/62/Adriana-Lima_Chgo_2010-12-10_photoby-Bielawski.jpg';
var fileTest = "test.jpg";
//var fileTestDownsized = "test_downsized.jpg";

var download = function(uri, filename, callback) {

  request.head(uri, function(err, res, body) {
    console.log('content-type:', res.headers['content-type']);
    
    request(uri).pipe(fs.createWriteStream(filename)).on('close', callback);
  });

};

download(uri, fileTest, function() {
  console.log('done, resizing...');
  //var writeStream = fs.createWriteStream(fileTestDownsized);
  //gm(fileTest).resize(620, 480).autoOrient().write(writeStream, function(err) {
  //  if (err)
  //	console.log(err);
  //});
});

