
var MongoClient = require('mongodb').MongoClient;
var http = require('http');

var mainUrl = "/tvapp/";
var channelsUrl = "channels";
var programmesUrl = "programmesSchedule";
var programmesNowUrl = "programmesNow";
var programmesOnIntervalUrl = "programmesOnInterval";
var categoriesUrl = "categories";
var port = 1337;
var servicePath = "channels";
var mongoUrl = "mongodb://localhost:27017/tvapp";

var onGetChannels = function(req, res) {

  MongoClient.connect(mongoUrl, function (err, db) {
    if (err) {
      res.writeHead(500, {'Content-Type':'application/json'});
      res.end("Error" + err.message);
    
    } else {
      
     var collection = db.collection('channels');
     collection.find().toArray(function(err, items) {
        if (err) {
          res.writeHead(500, {'Content-Type':'application/json'});
          res.end(error);
        } else {
          res.writeHead(200, {'Content-Type':'application/json'});
          res.end(JSON.stringify(items));
        }
      });
    }
  });
};

var onGetProgrammes = function(req, res) {

  MongoClient.connect(mongoUrl, function (err, db) {
    if (err) {
      res.writeHead(500, {'Content-Type':'application/json'});
      res.end("Error: " + err.message);
    
    } else {
      
     var collection = db.collection('programmes');
     collection.find().toArray(function(err, items) {
        if (err) {
          res.writeHead(500, {'Content-Type':'application/json'});
          res.end(error);
        } else {
          res.writeHead(200, {'Content-Type':'application/json'});
          res.end(JSON.stringify(items));
        }
      });
    }
  });
};

var onGetProgrammesOnInterval = function (req, res, timeStart, timeEnd) {

  MongoClient.connect(mongoUrl, function (err, db) {
    if (err) {
      res.writeHead(500, {'Content-Type':'application/json'});
      res.end("ERROR: " + err.message);
    } else {

     console.log("Time query start: " + timeStart + " end:" +  timeEnd);      
     var collection = db.collection('programmesScheduled');
     var query = {"start":{$lt:timeEnd}, "stop":{$gt:timeStart}};
     collection.find(query).toArray(function(err, items) {
        if (err) {
          res.writeHead(500, {'Content-Type':'application/json'});
          res.end(error);
        } else {
          res.writeHead(200, {'Content-Type':'application/json'});
          res.end(JSON.stringify(items));
        }
      });
    }
  });
}

var onGetProgrammesNow = function(req, res, timeQuery, categories) {

  MongoClient.connect(mongoUrl, function (err, db) {
    if (err) {
      res.writeHead(500, {'Content-Type':'application/json'});
      res.end("ERROR: " + err.message);
    } else {

     console.log("Time query: " + timeQuery);      
     var collection = db.collection('programmesScheduled');
     var query = {"start":{$lt:timeQuery}, "stop":{$gt:timeQuery}};
     collection.find(query).toArray(function(err, items) {
        if (err) {
          res.writeHead(500, {'Content-Type':'application/json'});
          res.end(error);
        } else {
          res.writeHead(200, {'Content-Type':'application/json'});
          res.end(JSON.stringify(items));
        }
      });
    }
  });
}

var onGetCategories = function(req, res) { 
 
  MongoClient.connect(mongoUrl, function (err, db) {
    if (err) {
      res.writeHead(500, {'Content-Type':'application/json'});
      res.end("ERROR: " + err.message);
    } else {

      console.log("Category query");      
      var collection = db.collection('categories');
      
      collection.find({}).toArray(function(err, items) {
        if (err) {
          res.writeHead(500, {'Content-Type':'application/json'});
          res.end(error);
        } else {
          res.writeHead(200, {'Content-Type':'application/json'});
          res.end(JSON.stringify(items));
        }
      });
    }
  });
}

http.createServer(function(req, res) {
  
  console.log('Request received: ' + req.method + " on:" + req.url);

  parsedRq = require('url').parse(req.url, true);
  var url = parsedRq.pathname;
  var query = parsedRq.query;

  if (url === mainUrl) {
    res.writeHead(200, {'Content-Type':'application/json'});
    res.end("OK");
  }

  if (url === mainUrl + channelsUrl) {
    if (req.method === 'GET') {
      onGetChannels(req, res);
    }
  }

  if (url == mainUrl + programmesUrl) {
    if (req.method === 'GET') {
      onGetProgrammes(req, res);
    }
  }
  
  if (url == mainUrl + programmesNowUrl) {
    if (req.method === 'GET') {
      onGetProgrammesNow(req, res, Number(query.date));
    }
  }

  if (url == mainUrl + programmesOnIntervalUrl) {
    if (req.method === 'GET') {
      onGetProgrammesOnInterval(req, res, Number(query.startTime), Number(query.endTime));
    }
  }

  if (url == mainUrl + categoriesUrl) {
    if (req.method === 'GET') {
      onGetCategories(req, res);
    }
  }

}).listen(port, '127.0.0.1');
console.log('Server running at http://127.0.0.1:' + port);
